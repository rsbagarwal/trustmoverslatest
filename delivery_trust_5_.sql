-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2021 at 01:14 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `delivery_trust`
--

-- --------------------------------------------------------

--
-- Table structure for table `boy_account_request`
--

CREATE TABLE `boy_account_request` (
  `id` int(11) NOT NULL,
  `boy_id` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-pending, 1-accepted, 2-rejected',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `boy_account_request`
--

INSERT INTO `boy_account_request` (`id`, `boy_id`, `status`, `date_time`, `updated_datetime`) VALUES
(1, 2, 2, '2020-01-21 09:39:31', '2020-01-21 09:39:31'),
(2, 4, 0, '2020-01-21 07:09:33', '2020-01-21 07:09:33'),
(6, 1, 1, '2020-10-14 01:06:38', '2020-10-14 01:06:38');

-- --------------------------------------------------------

--
-- Table structure for table `boy_signin_otp`
--

CREATE TABLE `boy_signin_otp` (
  `id` int(11) NOT NULL,
  `mobile` varchar(20) NOT NULL DEFAULT '',
  `otp` int(11) NOT NULL DEFAULT 0,
  `sent_count` int(11) NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `boy_signin_otp`
--

INSERT INTO `boy_signin_otp` (`id`, `mobile`, `otp`, `sent_count`, `date_time`) VALUES
(1, '+918627062095', 2479, 3, '2020-07-13 12:48:17'),
(2, '+919416879149', 2386, 1, '2020-07-15 03:52:14'),
(3, '+918847324981', 3777, 1, '2021-01-27 06:15:14'),
(4, '+919872904782', 2056, 3, '2020-11-24 11:10:35');

-- --------------------------------------------------------

--
-- Table structure for table `cash_max_limit`
--

CREATE TABLE `cash_max_limit` (
  `id` int(11) NOT NULL,
  `cash_limit` double NOT NULL DEFAULT 0,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_max_limit`
--

INSERT INTO `cash_max_limit` (`id`, `cash_limit`, `created_date`, `updated_date`) VALUES
(5, 100, '2019-12-12 04:37:13', '2020-06-01 11:47:40');

-- --------------------------------------------------------

--
-- Table structure for table `consignment_items`
--

CREATE TABLE `consignment_items` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` int(11) NOT NULL DEFAULT 0,
  `qty` int(11) NOT NULL DEFAULT 0,
  `description` varchar(255) NOT NULL DEFAULT ' ',
  `totalWeights` double NOT NULL DEFAULT 0,
  `width` double NOT NULL DEFAULT 0,
  `height` double NOT NULL DEFAULT 0,
  `length` double NOT NULL DEFAULT 0,
  `cubic_fair` double NOT NULL DEFAULT 0,
  `Unwrapping` int(11) NOT NULL DEFAULT 0,
  `totalPrice` double NOT NULL DEFAULT 0,
  `Installation` int(11) NOT NULL DEFAULT 0,
  `product_size` varchar(255) NOT NULL DEFAULT ' ',
  `autoRefrence` varchar(255) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `consignment_items`
--

INSERT INTO `consignment_items` (`id`, `vendor_id`, `order_id`, `reference`, `qty`, `description`, `totalWeights`, `width`, `height`, `length`, `cubic_fair`, `Unwrapping`, `totalPrice`, `Installation`, `product_size`, `autoRefrence`) VALUES
(110, 32, 352, 1001, 2, 'test', 0, 10, 10, 10, 2000, 1, 0, 1, 'small', 'TMF1001'),
(111, 32, 352, 1002, 0, 'test2', 0, 0, 0, 0, 0, 1, 0, 1, 'small', 'TMF1002');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_boys`
--

CREATE TABLE `delivery_boys` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT ' ',
  `email` varchar(255) NOT NULL DEFAULT ' ',
  `mobile` varchar(20) NOT NULL DEFAULT ' ',
  `password` varchar(255) NOT NULL DEFAULT ' ',
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-active, 1-inactive',
  `profile_image` varchar(500) NOT NULL DEFAULT ' ',
  `current_lat` double NOT NULL DEFAULT 0,
  `current_long` double NOT NULL DEFAULT 0,
  `transporter_type` varchar(100) NOT NULL DEFAULT '',
  `lname` varchar(255) NOT NULL DEFAULT ' ',
  `city` varchar(255) NOT NULL DEFAULT ' ',
  `vehicle_availbility` varchar(255) NOT NULL DEFAULT '  ',
  `vehicle_model` varchar(255) NOT NULL,
  `visa_type` varchar(255) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_boys`
--

INSERT INTO `delivery_boys` (`id`, `name`, `email`, `mobile`, `password`, `created_date`, `updated_date`, `status`, `profile_image`, `current_lat`, `current_long`, `transporter_type`, `lname`, `city`, `vehicle_availbility`, `vehicle_model`, `visa_type`) VALUES
(19, 'ankita', 'ankita@gmail.com', '+641234567898', '1234567', '2021-03-30 05:31:04', '2021-07-14 06:26:37', 0, '', 0, 0, '3 wheeler', 'singh', 'chandigarh', 'no', '1233', 'work vise'),
(20, 'aman', 'admin@gmail.com', '+641234567891', '1234567890', '2021-03-30 05:56:35', '2021-03-30 05:59:21', 0, '', 0, 0, '3 wheeler', 'singh', 'chandigarh', 'yes', '123', 'nz resident'),
(21, 'bhavik', 'abc@de.com', '+6480808080808080', '123456', '2021-03-30 13:10:45', '2021-03-30 13:10:45', 0, '', 0, 0, '2 wheeler', 'gujral', 'nz', 'yes', 'hr1234', 'Student Vise'),
(23, 'rahul', 'rahul@gmail.com', '+641234567899', '1234567', '2021-07-05 07:47:01', '2021-07-05 08:46:55', 0, '', 0, 0, '2 wheeler', 'singh', 'chandigarh', 'no', '22', 'Student Vise'),
(24, 'aman', 'aman@gmail.com', '+641234567897', '1234567', '2021-07-05 08:48:59', '2021-07-05 08:49:12', 0, '', 0, 0, 'E Bike', 'gujral', 'mohali', 'yes', '2255', 'work vise');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_boy_cash_deposit`
--

CREATE TABLE `delivery_boy_cash_deposit` (
  `id` int(11) NOT NULL,
  `transaction_id` varchar(255) NOT NULL DEFAULT ' ',
  `payment_mode` varchar(255) NOT NULL DEFAULT ' ',
  `paid_amount` double NOT NULL DEFAULT 0,
  `remark` varchar(255) NOT NULL DEFAULT ' ',
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `boy_id` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-not approved, 1-approved'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_boy_cash_deposit`
--

INSERT INTO `delivery_boy_cash_deposit` (`id`, `transaction_id`, `payment_mode`, `paid_amount`, `remark`, `created_date`, `boy_id`, `status`) VALUES
(1, 'dfddfdf23', 'cash', 7890, '', '2020-12-21 18:30:00', 0, 0),
(2, '567', 'cash', 5675, '', '2019-12-12 06:26:01', 0, 0),
(3, 'dfddfdf23', 'cash', 1234, '', '2019-12-14 05:26:45', 0, 0),
(4, '12345', 'cash', 567, '', '2020-01-20 10:59:37', 0, 0),
(5, '234', 'cash', 567, '', '2020-01-25 11:46:31', 1, 1),
(6, '31235465', 'paytm', 150, 'submitted done', '2020-02-07 13:11:28', 1, 0),
(7, '9584456', 'Paypal', 200, 'done payment', '2020-02-07 13:15:24', 1, 0),
(8, '6546586', 'Icici', 500, 'transaction done', '2020-02-07 13:16:44', 1, 0),
(9, '564655', 'SBI', 260, 'done', '2020-02-07 13:18:28', 1, 0),
(10, '654685', 'PNB', 120, 'done payment', '2020-02-07 13:19:39', 1, 0),
(11, '45672', 'phonepe', 500, '', '2020-04-24 13:29:11', 6, 1),
(12, '2345', 'paytm', 20, '', '2020-05-19 04:44:21', 6, 1),
(13, '34', 'paytm', 30, '', '2020-05-19 04:46:39', 6, 1),
(14, '345', 'paytm', 50, '', '2020-05-19 04:47:38', 6, 1),
(15, '98778', 'paytm', 20, '', '2020-05-19 04:49:21', 6, 1),
(16, '456', 'paytm', 30, '', '2020-05-19 04:50:04', 6, 1),
(17, '2345', 'paytm', 345677, '', '2020-05-19 04:51:03', 6, 1),
(18, '34', 'paytm', 23, '', '2020-06-11 10:34:51', 4, 1),
(19, '12334', 'paytm', 400, '', '2020-08-12 08:49:02', 4, 1),
(20, 'adsef', 'cash', 120, '455', '2020-12-23 09:12:52', 1, 1),
(21, 'abc', 'cash', 50, 'fgh', '2020-12-23 10:06:53', 11, 1),
(22, '1515', 'cash', 50, '', '2020-12-24 06:14:36', 11, 1),
(23, '1851', 'cash', 50, '', '2020-11-22 18:30:00', 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_boy_payout`
--

CREATE TABLE `delivery_boy_payout` (
  `id` int(11) NOT NULL,
  `transaction_id` varchar(255) NOT NULL DEFAULT ' ',
  `payment_mode` varchar(255) NOT NULL DEFAULT ' ',
  `paid_amount` double NOT NULL DEFAULT 0,
  `remark` varchar(255) NOT NULL DEFAULT ' ',
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `boy_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_boy_payout`
--

INSERT INTO `delivery_boy_payout` (`id`, `transaction_id`, `payment_mode`, `paid_amount`, `remark`, `created_date`, `boy_id`) VALUES
(2, '1234', 'cash', 23456, 'dvdfgdfgdfgdf', '2019-12-11 09:18:03', 0),
(3, '67', 'online', 23456, '3344', '2019-12-11 09:19:12', 0),
(4, '12234', 'online', 6789, '', '2019-12-11 10:16:12', 0),
(5, '456', '233', 123, '', '2019-12-11 10:38:37', 0),
(6, '1234', 'online', 456, '', '2019-12-11 11:28:48', 0),
(7, '123456', 'online', 6789, '', '2019-12-12 06:05:02', 0),
(8, '1234', 'cash', 324, 'dvdfgdfgdfgdf', '2019-12-13 12:09:47', 0),
(9, '12345', 'online', 678, '', '2020-01-25 11:51:41', 1),
(10, '98765678', 'paytm', 600, '', '2020-04-24 13:23:21', 6),
(11, '76866', 'paytm', 500, '', '2020-04-24 13:24:18', 6),
(12, '123453', 'paytm', 34, '', '2020-06-11 10:34:30', 6);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_boy_signup_requests`
--

CREATE TABLE `delivery_boy_signup_requests` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT ' ',
  `mobile` varchar(20) NOT NULL DEFAULT ' ',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-pending, 1-created, 2-rejected',
  `request_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_boy_signup_requests`
--

INSERT INTO `delivery_boy_signup_requests` (`id`, `email`, `mobile`, `status`, `request_datetime`, `updated_datetime`, `name`) VALUES
(35, 'anja@gmail.com', '+91123', 0, '2021-05-18 12:16:24', '2021-05-18 12:16:24', 'anjana'),
(36, 'anja@gmail.com', '+911234567890', 0, '2021-07-02 07:03:12', '2021-07-02 07:03:12', 'anjana'),
(37, 'anjaaa@gmail.com', '+9112345', 0, '2021-07-02 09:39:07', '2021-07-02 09:39:07', 'ankita'),
(38, 'anjaaa@gmail.com', '+9112345', 0, '2021-07-02 09:40:17', '2021-07-02 09:40:17', 'ankita'),
(39, 'ankita@gmail.com', '+91123456', 0, '2021-07-02 09:43:12', '2021-07-02 09:43:12', 'ankita'),
(40, 'ankita2@gmailcom', '+911234567', 0, '2021-07-02 09:44:43', '2021-07-02 09:44:43', 'ankita');

-- --------------------------------------------------------

--
-- Table structure for table `online_orders_assign`
--

CREATE TABLE `online_orders_assign` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT 0 COMMENT 'reference to vendor table',
  `pickup_address` varchar(255) NOT NULL DEFAULT ' ',
  `pickup_lat` double NOT NULL DEFAULT 0,
  `pickup_long` double NOT NULL DEFAULT 0,
  `drop_address` varchar(255) NOT NULL DEFAULT ' ',
  `drop_lat` double NOT NULL DEFAULT 0,
  `drop_long` double NOT NULL DEFAULT 0,
  `pickup_time` varchar(20) NOT NULL DEFAULT ' ',
  `delivery_datetime` varchar(100) NOT NULL DEFAULT '',
  `delivery_charges` double NOT NULL DEFAULT 0 COMMENT 'in rs',
  `delivery_boy_id` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-pending,1-Not picked,2-picked up, 3-delivered, 4- assign to delivery boy but pending to accept on boy side',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `pickup_datetime` varchar(100) NOT NULL,
  `update_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `boy_assign_datetime` varchar(100) NOT NULL DEFAULT '',
  `remark` varchar(500) NOT NULL DEFAULT '',
  `estimated_distance` double NOT NULL DEFAULT 0 COMMENT 'in km',
  `tracking_id` varchar(100) NOT NULL DEFAULT '',
  `customer_name` varchar(255) NOT NULL DEFAULT '',
  `customer_mobile` varchar(20) NOT NULL DEFAULT '',
  `payment_amount` double NOT NULL DEFAULT 0,
  `pickup_img` varchar(500) NOT NULL DEFAULT '',
  `drop_img` varchar(500) NOT NULL DEFAULT '',
  `drophouseno` varchar(200) NOT NULL DEFAULT '',
  `pickuphouseno` varchar(200) NOT NULL DEFAULT '',
  `extra_charges` double NOT NULL DEFAULT 0,
  `floor` int(11) DEFAULT 0,
  `stairsLift` int(11) DEFAULT 0,
  `delivery_item` varchar(255) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `online_orders_assign`
--

INSERT INTO `online_orders_assign` (`id`, `vendor_id`, `pickup_address`, `pickup_lat`, `pickup_long`, `drop_address`, `drop_lat`, `drop_long`, `pickup_time`, `delivery_datetime`, `delivery_charges`, `delivery_boy_id`, `status`, `date_time`, `pickup_datetime`, `update_datetime`, `boy_assign_datetime`, `remark`, `estimated_distance`, `tracking_id`, `customer_name`, `customer_mobile`, `payment_amount`, `pickup_img`, `drop_img`, `drophouseno`, `pickuphouseno`, `extra_charges`, `floor`, `stairsLift`, `delivery_item`) VALUES
(352, 32, 'Meshinkhub Mohali', 30.6639722, 76.8154888, 'chandigarh', 30.7333148, 76.7794179, '', '2021-07-23 04:40', 1500, 0, 0, '2021-07-22 22:57:47', '2021-07-23 05:20', '2021-07-22 22:57:47', '', 'good', 10, 'TM352', 'anjana', '+648989898989', 0, '', '', '', '', 0, 1, 1, 'Boxes');

-- --------------------------------------------------------

--
-- Table structure for table `otp_mobile`
--

CREATE TABLE `otp_mobile` (
  `id` bigint(20) NOT NULL,
  `mobile_no` varchar(14) NOT NULL DEFAULT '',
  `otp` varchar(5) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0 -not confirm, 1- confirmed',
  `date_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otp_mobile`
--

INSERT INTO `otp_mobile` (`id`, `mobile_no`, `otp`, `status`, `date_time`) VALUES
(3, '+919416879149', '4449', 0, '2020-07-03 10:44:22'),
(4, '+919416879149', '1247', 1, '2020-07-03 10:48:59'),
(5, '+919416879149', '1189', 0, '2020-07-03 12:10:23'),
(6, '+919416879149', '1736', 1, '2020-07-03 12:12:59'),
(7, '+919416879149', '1064', 1, '2020-07-03 12:14:20'),
(8, '+919416879149', '1459', 1, '2020-07-03 12:19:30'),
(9, '+919416879149', '1336', 1, '2020-07-03 12:24:50'),
(10, '+919416879149', '1820', 1, '2020-07-03 12:26:53'),
(11, '+919416879149', '1747', 1, '2020-07-03 12:28:18'),
(12, '+919416879149', '1787', 1, '2020-07-03 12:32:22'),
(13, '+919416879149', '1439', 1, '2020-07-03 12:34:08'),
(14, '+918627062095', '2879', 1, '2020-07-06 03:40:58'),
(15, '+918627062095', '1746', 1, '2020-07-06 04:21:43'),
(16, '+918627062095', '2897', 1, '2020-07-06 04:23:03'),
(17, '+918627062095', '4411', 1, '2020-07-06 04:51:26'),
(18, '+918627062095', '1664', 1, '2020-07-06 04:52:53'),
(19, '+918627062095', '1536', 1, '2020-07-06 04:54:02'),
(20, '+918627062095', '3121', 1, '2020-07-06 05:04:10'),
(21, '+918627062095', '5088', 1, '2020-07-06 05:13:38'),
(22, '+918627062095', '5152', 1, '2020-07-06 06:19:04'),
(23, '+918627062095', '3161', 1, '2020-07-06 06:20:25'),
(24, '+918627062095', '8087', 1, '2020-07-06 06:22:01'),
(25, '+918627062095', '6523', 1, '2020-07-06 06:27:06'),
(26, '+918627062095', '7597', 1, '2020-07-06 06:27:40'),
(27, '+918627062095', '9058', 1, '2020-07-06 06:30:18'),
(28, '+918627062095', '8277', 0, '2020-07-06 06:31:08'),
(29, '+918627062095', '1217', 0, '2020-07-06 06:55:40'),
(30, '+918627062095', '3839', 1, '2020-07-06 06:56:17'),
(31, '+918627062095', '5640', 1, '2020-07-06 06:58:29'),
(32, '+918627062095', '1182', 1, '2020-07-06 07:00:09'),
(33, '+918627062095', '7355', 1, '2020-07-06 07:00:44'),
(34, '+918627062095', '8427', 1, '2020-07-06 07:03:01'),
(35, '+918627062095', '1000', 1, '2020-07-06 07:03:37'),
(36, '+918627062095', '6098', 1, '2020-07-06 07:06:42'),
(37, '+918627062095', '1107', 1, '2020-07-06 07:11:39'),
(38, '+918627062095', '9416', 1, '2020-07-06 07:31:06'),
(39, '+918627062095', '8559', 1, '2020-07-06 07:37:49'),
(40, '+918627062095', '7138', 1, '2020-07-06 07:40:09'),
(41, '+918627062095', '6749', 1, '2020-07-06 07:40:49'),
(42, '+918627062095', '3418', 1, '2020-07-06 07:43:38'),
(43, '+918627062095', '1140', 1, '2020-07-06 07:45:05'),
(44, '+918627062095', '1426', 1, '2020-07-06 07:58:49'),
(45, '+918627062095', '1227', 1, '2020-07-06 08:00:17'),
(46, '+918627062095', '1558', 1, '2020-07-06 08:01:44'),
(47, '+918627062095', '1615', 1, '2020-07-08 05:37:57'),
(48, '+918627062095', '4847', 1, '2020-07-09 05:53:01'),
(49, '+918627062095', '8167', 0, '2020-07-09 09:15:02'),
(50, '+918627062095', '1881', 0, '2020-07-09 09:18:53'),
(51, '+918627062095', '1698', 0, '2020-07-09 09:20:47'),
(52, '+918627062095', '1533', 0, '2020-07-09 09:22:15'),
(53, '+918627062095', '1813', 0, '2020-07-09 09:23:41'),
(54, '+918627062095', '9540', 0, '2020-07-09 09:24:45'),
(55, '+918627062095', '2743', 0, '2020-07-09 09:26:59'),
(56, '+918627062095', '8048', 0, '2020-07-09 09:38:42'),
(57, '+918627062095', '1556', 0, '2020-07-09 09:38:41'),
(58, '+918627062095', '1756', 0, '2020-07-09 09:38:07'),
(59, '+918627062095', '1456', 0, '2020-07-09 09:38:40'),
(60, '+918627062095', '4270', 0, '2020-07-09 09:39:27'),
(61, '+918627062095', '2156', 0, '2020-07-09 09:42:00'),
(62, '+918627062095', '1635', 0, '2020-07-09 09:42:06'),
(63, '+918627062095', '1719', 0, '2020-07-09 09:42:13'),
(64, '+918627062095', '9021', 0, '2020-07-09 09:42:24'),
(65, '+918627062095', '2062', 0, '2020-07-09 09:43:30'),
(66, '+918627062095', '4788', 0, '2020-07-09 09:46:36'),
(67, '+918627062095', '5370', 1, '2020-07-09 09:49:36'),
(68, '+918627062095', '2438', 0, '2020-07-10 11:42:44'),
(69, '+918627062095', '2592', 1, '2020-07-14 04:14:48'),
(70, '+918627062095', '6160', 1, '2020-07-14 04:22:19'),
(71, '+918627062095', '2432', 1, '2020-07-14 13:48:20'),
(72, '+918627062095', '2679', 1, '2020-07-14 13:54:00'),
(73, '+918627062095', '2102', 1, '2020-07-14 13:56:57'),
(74, '+918627062095', '1794', 1, '2020-07-14 13:59:17'),
(75, '+918627062095', '1034', 1, '2020-07-14 14:02:50'),
(76, '+918627062095', '3571', 1, '2020-07-14 14:04:30'),
(77, '+918627062095', '4035', 1, '2020-07-14 14:05:59'),
(78, '+918627062095', '8818', 1, '2020-07-14 14:08:24'),
(79, '+918627062095', '8676', 1, '2020-07-15 13:09:45'),
(80, '+918627062095', '5853', 1, '2020-07-15 13:13:42'),
(81, '+918627062095', '1629', 1, '2020-07-15 13:16:07'),
(82, '+918627062095', '2239', 1, '2020-07-15 15:10:39'),
(83, '+918627062095', '1508', 1, '2020-07-16 03:44:32'),
(84, '+918627062095', '1046', 1, '2020-07-16 03:45:39'),
(85, '+918627062095', '4585', 1, '2020-07-16 03:49:30'),
(86, '+918627062095', '2579', 0, '2020-07-16 08:33:15'),
(87, '+918627062095', '1697', 0, '2020-07-16 08:33:43'),
(88, '+918627062095', '1479', 1, '2020-07-16 08:34:42'),
(89, '+918627062095', '2295', 1, '2020-07-16 11:35:32'),
(90, '+918627062095', '1023', 1, '2020-07-16 11:57:17'),
(91, '+918627062095', '1377', 0, '2020-07-16 11:57:35'),
(92, '+918627062095', '7502', 0, '2020-07-16 11:57:42'),
(93, '+918627062095', '1414', 0, '2020-07-16 11:57:54'),
(94, '+918627062095', '1776', 0, '2020-07-16 11:58:12'),
(95, '+918627062095', '4963', 0, '2020-07-16 11:59:23'),
(96, '+918627062095', '1487', 0, '2020-07-16 11:59:58'),
(97, '+918627062095', '3322', 0, '2020-07-16 12:00:05'),
(98, '+918627062095', '1136', 0, '2020-07-16 12:01:50'),
(99, '+918627062095', '9018', 0, '2020-07-16 12:15:16'),
(100, '+918627062095', '1433', 1, '2020-07-22 04:02:02'),
(101, '+918627062095', '4506', 1, '2020-07-22 04:04:40'),
(102, '+918627062095', '1192', 1, '2020-07-22 04:09:44'),
(103, '+918219113123', '3253', 1, '2020-07-22 04:35:03'),
(104, '+918219113123', '1911', 1, '2020-07-22 04:35:31'),
(105, '+918219113123', '1751', 1, '2020-07-22 04:51:24'),
(106, '+918219113123', '6856', 1, '2020-07-22 04:53:29'),
(107, '+918219113123', '1118', 1, '2020-07-22 05:04:37'),
(108, '+918219113123', '1088', 1, '2020-07-22 05:18:27'),
(109, '+918219113123', '1144', 1, '2020-07-22 06:51:29'),
(110, '+918219113123', '9769', 1, '2020-07-22 06:53:20'),
(111, '+918219113123', '7225', 1, '2020-07-22 06:55:30'),
(112, '+918219113123', '1266', 1, '2020-07-22 07:03:34'),
(113, '+918219113123', '9453', 1, '2020-07-22 07:14:09'),
(114, '+918627062095', '2014', 1, '2020-07-22 07:20:03'),
(115, '+918219113123', '5907', 1, '2020-07-22 07:51:47'),
(116, '+918219113123', '2343', 1, '2020-07-22 08:30:56'),
(117, '+918627062095', '2645', 1, '2020-07-22 11:48:22'),
(118, '+918219113123', '8504', 1, '2020-07-27 06:13:02'),
(119, '+918219113123', '3301', 1, '2020-07-27 09:12:55'),
(120, '+918219113123', '3985', 1, '2020-07-27 10:00:51'),
(121, '+918627062095', '6038', 1, '2020-08-05 04:21:48'),
(122, '+919416879149', '8903', 1, '2020-08-10 11:58:50'),
(123, '+918627062095', '2512', 1, '2020-08-11 11:06:00'),
(124, '+918627062095', '2726', 1, '2020-08-11 13:58:51'),
(125, '+918627062095', '2136', 1, '2020-08-12 05:55:40'),
(126, '+918627062095', '6969', 1, '2020-08-18 06:23:26'),
(127, '+919872904782', '7790', 1, '2020-10-05 10:06:41'),
(128, '+919872904782', '1612', 0, '2020-10-05 10:12:23'),
(129, '+919872904782', '4082', 0, '2020-10-05 10:14:40'),
(130, '+919872904782', '6353', 1, '2020-10-05 10:15:00'),
(131, '+919872904782', '5395', 0, '2020-10-05 10:16:30'),
(132, '+919872904782', '1069', 0, '2020-10-05 10:16:41'),
(133, '+919872904782', '8091', 0, '2020-10-05 10:31:01'),
(134, '+919872904782', '1467', 0, '2020-10-05 10:31:23'),
(135, '+919872904782', '7674', 0, '2020-10-05 10:33:03'),
(136, '+919872904782', '1199', 0, '2020-10-05 10:33:34'),
(137, '+919872904782', '1202', 0, '2020-10-05 10:41:04'),
(138, '+919872904782', '1392', 0, '2020-10-05 10:42:05'),
(139, '+919872904782', '3348', 1, '2020-10-05 10:53:43'),
(140, '+919872904782', '9366', 0, '2020-10-05 10:55:59'),
(141, '+919872904782', '4386', 1, '2020-10-05 10:56:34'),
(142, '+919872904782', '4668', 1, '2020-10-05 10:58:55'),
(143, '+919872904782', '1787', 0, '2020-10-05 11:05:08'),
(144, '+919872904782', '9094', 1, '2020-10-05 11:05:20'),
(145, '+919872904782', '2241', 1, '2020-10-05 11:10:46'),
(146, '+919872904782', '1464', 1, '2020-10-05 11:18:51'),
(147, '+919872904782', '1604', 1, '2020-10-05 11:21:22'),
(148, '+919872904782', '5342', 1, '2020-10-05 11:27:29'),
(149, '+919872904782', '6829', 1, '2020-10-05 11:29:31'),
(150, '+919872904782', '1032', 0, '2020-10-05 11:32:09'),
(151, '+919872904782', '2202', 0, '2020-10-05 11:33:13'),
(152, '+919872904782', '1588', 1, '2020-10-05 11:34:37'),
(153, '+917807092589', '1063', 1, '2021-02-26 09:33:35'),
(154, '+917807092589', '1464', 0, '2021-02-26 10:08:02'),
(155, '+918556894136', '2250', 0, '2021-02-26 10:44:21'),
(156, '+918556894136', '1855', 1, '2021-02-26 10:44:51');

-- --------------------------------------------------------

--
-- Table structure for table `payment_logs`
--

CREATE TABLE `payment_logs` (
  `id` int(11) NOT NULL,
  `vendor_name` varchar(255) NOT NULL DEFAULT '',
  `vendor_id` int(11) NOT NULL DEFAULT 0,
  `vendor_mobile_no` varchar(255) NOT NULL DEFAULT ' ',
  `bill_amount` double NOT NULL DEFAULT 0,
  `payed_amount` double NOT NULL DEFAULT 0,
  `pending_amount` double NOT NULL DEFAULT 0,
  `payment_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `extra_charges` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_logs`
--

INSERT INTO `payment_logs` (`id`, `vendor_name`, `vendor_id`, `vendor_mobile_no`, `bill_amount`, `payed_amount`, `pending_amount`, `payment_time`, `extra_charges`) VALUES
(5, 'amren', 30, '+91123098765', 121, 21, 100, '2021-05-14 12:26:36', 0),
(6, 'amren', 30, '+91123098765', 200, 100, 100, '2021-05-14 12:38:02', 0);

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `id` int(11) NOT NULL,
  `message` varchar(1000) NOT NULL DEFAULT ' ',
  `send_type` int(11) NOT NULL DEFAULT 0 COMMENT '1- all drivers, 2-location based',
  `location_address` varchar(1000) NOT NULL DEFAULT ' ',
  `location_latitude` double NOT NULL DEFAULT 0,
  `location_longitude` double NOT NULL DEFAULT 0,
  `radius` double NOT NULL DEFAULT 0,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-not sent, 1-sent'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`id`, `message`, `send_type`, `location_address`, `location_latitude`, `location_longitude`, `radius`, `date_time`, `status`) VALUES
(1, 'Language, a system of conventional spoken, manual, or written symbols by means of which human beings, as members of a social group and participants in its culture, express themselves. The functions of language include communication, the expression of identity, play, imaginative ', 1, '', 0, 0, 0, '2019-12-12 07:25:40', 1),
(2, 'a verbal, written, or recorded communication sent to or left for a recipient who cannot be contacted directly.', 2, 'zirakpur', 456, 345, 0.5, '2019-12-12 07:41:57', 1),
(3, 'sdfgdfgdf', 1, '', 0, 0, 0, '2019-12-12 07:43:11', 0),
(4, 'asdasda', 1, '', 0, 0, 0, '2019-12-14 05:08:15', 0),
(5, 'asdasdas', 2, 'Zirakpur', 30.64254959999999, 76.81733589999999, 0.5, '2019-12-14 05:08:41', 0),
(6, 'sdafsdfs', 2, 'Zirakpur, Punjab, India', 30.64254959999999, 76.81733589999999, 0.5, '2019-12-14 05:14:51', 0),
(7, 'fdsfsdfsdf', 2, 'Zirakpur, Punjab, India', 30.64254959999999, 76.81733589999999, 0.5, '2019-12-14 05:30:07', 0),
(8, 'gvhgjhkjhhf', 2, 'Punjab, India', 31.14713050000001, 75.34121789999995, 0.5, '2019-12-14 05:43:58', 0),
(9, 'hello', 1, '', 0, 0, 0, '2019-12-30 11:31:48', 0),
(10, '5677', 1, '', 0, 0, 0, '2019-12-30 12:14:15', 0),
(11, 'tyuyu', 1, '', 0, 0, 0, '2019-12-30 12:25:20', 0),
(12, 'dfgdfg', 1, '', 0, 0, 0, '2020-01-01 10:48:32', 0),
(13, 'dffd', 1, '', 0, 0, 0, '2020-01-01 10:50:01', 0),
(14, 'Hello, Ashish', 1, '', 0, 0, 0, '2020-01-01 10:56:25', 0),
(15, 'hello', 1, '', 0, 0, 0, '2020-01-01 11:08:17', 0),
(16, 'hello', 1, '', 0, 0, 0, '2020-01-01 11:14:01', 0),
(17, 'hello everyone', 1, '', 0, 0, 0, '2020-01-01 11:32:49', 0),
(18, 'Hiiii', 1, '', 0, 0, 0, '2020-01-01 11:35:53', 0),
(19, 'Hello', 1, '', 0, 0, 0, '2020-01-22 08:32:49', 0),
(20, 'Helo', 1, '', 0, 0, 0, '2020-01-22 08:34:12', 0),
(21, 'hii', 1, '', 0, 0, 0, '2020-01-22 09:15:06', 0),
(22, 'hiii', 1, '', 0, 0, 0, '2020-01-22 09:16:23', 0),
(23, 'grr', 1, '', 0, 0, 0, '2020-01-22 09:19:32', 0),
(24, 'hiii', 1, '', 0, 0, 0, '2020-01-22 09:21:45', 0),
(25, 'ghii', 1, '', 0, 0, 0, '2020-01-22 09:24:39', 0),
(26, 'hello', 1, '', 0, 0, 0, '2020-01-22 09:25:06', 0),
(27, 'fggh', 1, '', 0, 0, 0, '2020-01-22 09:28:25', 0),
(28, 'hiii', 1, '', 0, 0, 0, '2020-01-22 09:30:58', 0),
(29, 'hello', 1, '', 0, 0, 0, '2020-01-22 09:32:07', 0),
(30, 'hiii', 1, '', 0, 0, 0, '2020-01-22 09:36:05', 0),
(31, 'hiii', 1, '', 0, 0, 0, '2020-01-22 09:36:31', 0),
(32, 'hiii', 1, '', 0, 0, 0, '2020-01-22 09:37:13', 0),
(33, 'hello', 1, '', 0, 0, 0, '2020-01-22 09:40:28', 0),
(34, 'heee', 1, '', 0, 0, 0, '2020-01-22 09:42:04', 0),
(35, 'hii', 1, '', 0, 0, 0, '2020-01-22 09:46:47', 0),
(36, 'hiii', 1, '', 0, 0, 0, '2020-01-22 09:51:01', 0),
(37, 'hiiii', 1, '', 0, 0, 0, '2020-01-22 09:51:42', 0),
(38, 'hiii', 1, '', 0, 0, 0, '2020-01-22 09:55:43', 0),
(39, 'hello', 1, '', 0, 0, 0, '2020-01-22 09:57:07', 0),
(40, 'hiii', 1, '', 0, 0, 0, '2020-01-22 09:58:49', 0),
(41, 'messs', 1, '', 0, 0, 0, '2020-01-22 10:00:58', 0),
(42, 'hii', 1, '', 0, 0, 0, '2020-01-22 10:02:17', 0),
(43, 'dfdfhdfgd', 1, '', 0, 0, 0, '2020-01-22 10:08:49', 0),
(44, 'ghjhjgfjfgf', 1, '', 0, 0, 0, '2020-01-22 10:09:24', 0),
(45, 'helllo', 1, '', 0, 0, 0, '2020-01-22 10:09:55', 0),
(46, 'hello', 1, '', 0, 0, 0, '2020-01-22 10:10:24', 0),
(47, 'hii', 1, '', 0, 0, 0, '2020-01-22 10:11:15', 0),
(48, 'hiiiii', 1, '', 0, 0, 0, '2020-01-22 10:12:29', 0),
(49, 'hello', 1, '', 0, 0, 0, '2020-01-22 10:37:38', 0),
(50, 'hiii', 1, '', 0, 0, 0, '2020-01-22 10:59:39', 0),
(51, 'hiii', 1, '', 0, 0, 0, '2020-01-22 11:00:14', 0),
(52, 'AAAAAAAAAAAAAAAAAAAAAAA', 2, 'Mohali punjab india ', 30.7046486, 76.7178726, 5, '2020-10-13 17:53:38', 0),
(53, 'BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB', 2, 'Mohali punjab india ', 30.7046486, 76.7178726, 5, '2020-10-13 18:02:22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `push_messages_history`
--

CREATE TABLE `push_messages_history` (
  `id` bigint(20) NOT NULL,
  `delivery_boy_id` int(11) NOT NULL DEFAULT 0,
  `push_message_id` int(11) NOT NULL DEFAULT 0,
  `message` varchar(1000) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `read_message` int(11) NOT NULL DEFAULT 0 COMMENT '0-not read, 1-read'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages_history`
--

INSERT INTO `push_messages_history` (`id`, `delivery_boy_id`, `push_message_id`, `message`, `date_time`, `read_message`) VALUES
(1, 1, 1, 'Language, a system of conventional spoken, manual, or written symbols by means of which human beings, as members of a social group and participants in its culture, express themselves. The functions of language include communication, the expression of identity, play, imaginative ', '2019-12-12 07:25:40', 1),
(2, 1, 2, 'a verbal, written, or recorded communication sent to or left for a recipient who cannot be contacted directly.', '2019-12-12 07:41:57', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles_access`
--

CREATE TABLE `roles_access` (
  `id` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT 0 COMMENT '1-MF Team, 2-Restaurant',
  `restaurant_id` int(11) NOT NULL DEFAULT 0,
  `email` varchar(255) NOT NULL DEFAULT ' ',
  `password` varchar(255) NOT NULL DEFAULT ' ',
  `access` varchar(1000) NOT NULL DEFAULT ' ',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-active, 1-inactive',
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles_access`
--

INSERT INTO `roles_access` (`id`, `role`, `restaurant_id`, `email`, `password`, `access`, `status`, `created_datetime`, `updated_datetime`) VALUES
(1, 1, 0, 'admin@gmail.com', 'admin@gmail.com', '', 0, '2019-12-12 12:40:45', '2019-12-13 04:36:21'),
(2, 1, 0, 'thakur@gmail.com', '1234567', 'dashboard,orderHistory,restaurants,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage', 0, '2019-12-12 12:43:05', '2019-12-13 08:16:05'),
(6, 1, 0, 'thakur1@gmail.com', '1234567', '', 1, '2019-12-12 13:40:52', '2019-12-13 04:32:14'),
(7, 1, 0, 'thakur81@gmail.com', '1234567', '', 0, '2019-12-12 13:42:10', '2019-12-12 13:42:10'),
(8, 1, 0, 'thakur45@gmail.com', '1234567', '', 0, '2019-12-12 13:43:01', '2019-12-12 13:43:01'),
(9, 1, 0, 'thakur15@gmail.com', '1234567', '', 0, '2019-12-12 13:43:40', '2019-12-12 13:43:40'),
(10, 2, 227, '', '', 'orderHistory ', 0, '2019-12-13 06:04:03', '2019-12-13 06:04:03'),
(11, 2, 34557, '', '', 'orderHistory ', 0, '2019-12-13 06:07:09', '2019-12-13 06:07:09'),
(12, 1, 0, 'thakur456@gmail.com', '123456789', 'dashboard,deliveryBoy,orderHistory,restaurants,deliveryBoyPayout,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage', 0, '2019-12-13 08:18:34', '2019-12-13 08:18:34'),
(13, 1, 0, 'thakur341@gmail.com', '123456', 'dashboard,deliveryBoy,orderHistory,restaurants,deliveryBoyPayout,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage', 0, '2019-12-13 08:19:11', '2019-12-13 08:19:11'),
(14, 1, 0, 'thakur134546@gmail.com', '12345678', 'dashboard,deliveryBoy,orderHistory,restaurants,deliveryBoyPayout,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage', 0, '2019-12-13 08:26:02', '2019-12-13 08:26:02'),
(15, 1, 0, 'thakur156@gmail.com', '1234567', 'orderHistory,restaurants,deliveryBoyPayout,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage', 0, '2019-12-13 08:27:27', '2019-12-13 08:27:27'),
(16, 1, 0, 'thakur145@gmail.com', '123456', 'dashboard,deliveryBoy,orderHistory,restaurants,deliveryBoyPayout,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage', 0, '2019-12-13 08:30:11', '2019-12-13 08:30:11'),
(17, 1, 0, 'thakur1234@gmail.com', '12345678', 'deliveryBoy,orderHistory,restaurants,cashDepositByDeliveryBoy,pushMessage', 0, '2019-12-14 12:13:09', '2020-01-23 06:07:00'),
(18, 1, 0, 'thakur12@gmail.com', '1234567', 'dashboard,deliveryBoy,orderHistory,restaurants,deliveryBoyPayout,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage', 0, '2020-01-23 07:05:30', '2020-01-23 07:05:30'),
(19, 1, 0, 'jevanthakur@gmail.com', '1234567', 'dashboard,deliveryBoy,orderHistory', 0, '2020-02-27 10:10:27', '2020-02-27 10:10:27'),
(20, 1, 0, 'jevanthakur3@gmail.com', '12345678', 'dashboard,deliveryBoy,orderHistory,restaurants,deliveryBoyPayout,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage,rolesAccess,accountDeactivation,signupRequest', 0, '2020-02-27 10:23:43', '2020-02-27 10:35:09'),
(21, 1, 0, 'a1dmin@gmail.com', '1234567', 'dashboard,deliveryBoy,orderHistory,restaurants,deliveryBoyPayout,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage,rolesAccess,accountDeactivation,signupRequest', 0, '2020-10-14 00:03:31', '2020-10-14 00:03:31'),
(22, 1, 0, 'admin1@gmail.com', 'admin@gmail.com', 'dashboard,deliveryBoy,orderHistory,restaurants,deliveryBoyPayout,cashDeliveryBoy,cashDepositByDeliveryBoy,pushMessage,rolesAccess,accountDeactivation,signupRequest', 0, '2020-12-01 07:52:06', '2020-12-01 07:52:06'),
(23, 2, 3455, '', '', 'orderHistory ', 0, '2020-12-01 07:55:22', '2020-12-01 07:55:46');

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE `rules` (
  `id` int(11) NOT NULL,
  `Km` double NOT NULL DEFAULT 0,
  `fare_per_km` double NOT NULL DEFAULT 0,
  `vendor_id` int(11) NOT NULL DEFAULT 0,
  `Km_to` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rules`
--

INSERT INTO `rules` (`id`, `Km`, `fare_per_km`, `vendor_id`, `Km_to`) VALUES
(72, 1, 100, 32, 20),
(73, 21, 200, 32, 30),
(74, 0, 100, 72, 100),
(75, 0, 100, 73, 20);

-- --------------------------------------------------------

--
-- Table structure for table `seller_login`
--

CREATE TABLE `seller_login` (
  `id` int(11) NOT NULL,
  `restaurant_name` varchar(200) NOT NULL DEFAULT ' '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `switch_delivery_service_request`
--

CREATE TABLE `switch_delivery_service_request` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL DEFAULT 0,
  `start_date` varchar(50) NOT NULL DEFAULT '',
  `end_date` varchar(50) NOT NULL DEFAULT '',
  `start_time` varchar(50) NOT NULL DEFAULT '',
  `end_time` varchar(50) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 0 COMMENT '0-pending,1-accepted,2-rejected',
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `switch_delivery_service_request`
--

INSERT INTO `switch_delivery_service_request` (`id`, `seller_id`, `start_date`, `end_date`, `start_time`, `end_time`, `status`, `created_datetime`, `updated_datetime`) VALUES
(6, 227, '27-05-2020', '30-05-2020', '05:08', '11:30', 1, '2020-05-27 04:00:10', '2020-05-27 05:29:18'),
(8, 227, '03-06-2020', '10-06-2020', '11:42', '23:42', 0, '2020-06-02 05:12:54', '2020-06-02 05:12:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `mobile` varchar(15) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `name`, `email`, `mobile`, `password`, `datetime`) VALUES
(1, 'Admin', 'admin99@gmail.com', '+918213412121', '1234567', '2020-06-30 10:12:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seller_service_bit`
--

CREATE TABLE `tbl_seller_service_bit` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `email_parsor_bit` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `vendor_name` varchar(500) NOT NULL DEFAULT '',
  `address` varchar(500) NOT NULL DEFAULT '',
  `mobile` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `created_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `wallet_amount` double NOT NULL DEFAULT 0,
  `latitude` double NOT NULL DEFAULT 0,
  `longitude` double NOT NULL DEFAULT 0,
  `password` varchar(50) NOT NULL DEFAULT '',
  `base_fare` double NOT NULL DEFAULT 0 COMMENT 'in rupees',
  `distance_for_basefare` double NOT NULL DEFAULT 0 COMMENT 'in km',
  `aftr_base_dist_per_km_charge` double NOT NULL DEFAULT 0 COMMENT 'in rupees',
  `t_c_accept` int(11) NOT NULL DEFAULT 0,
  `nearest_address` varchar(500) NOT NULL DEFAULT '',
  `billing_amount` double NOT NULL DEFAULT 0,
  `small_item` double(40,2) NOT NULL,
  `medium_item` double(40,2) NOT NULL DEFAULT 0.00,
  `large_item` double(40,2) NOT NULL DEFAULT 0.00,
  `extraLarge_item` double(40,2) NOT NULL DEFAULT 0.00,
  `installation_fare` double(40,2) NOT NULL DEFAULT 0.00,
  `unwrapping_fare` double(40,2) NOT NULL DEFAULT 0.00,
  `floor` int(11) NOT NULL DEFAULT 0,
  `floor_fair` double(40,2) NOT NULL DEFAULT 0.00,
  `stair_fair` double(40,2) NOT NULL DEFAULT 0.00,
  `lift_fair` double(40,2) NOT NULL DEFAULT 0.00,
  `ground_fair` double(40,2) NOT NULL,
  `thirty_km` double(40,2) NOT NULL DEFAULT 0.00,
  `sixty_km` double(40,2) NOT NULL DEFAULT 0.00,
  `thundred_km` double(40,2) NOT NULL DEFAULT 0.00,
  `hourly_rate` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `vendor_name`, `address`, `mobile`, `email`, `status`, `created_datetime`, `updated_datetime`, `wallet_amount`, `latitude`, `longitude`, `password`, `base_fare`, `distance_for_basefare`, `aftr_base_dist_per_km_charge`, `t_c_accept`, `nearest_address`, `billing_amount`, `small_item`, `medium_item`, `large_item`, `extraLarge_item`, `installation_fare`, `unwrapping_fare`, `floor`, `floor_fair`, `stair_fair`, `lift_fair`, `ground_fair`, `thirty_km`, `sixty_km`, `thundred_km`, `hourly_rate`) VALUES
(30, 'amren', 'mohali', '+64123098765', 'amren@gmail.com', 3, '2021-05-11 06:31:17', '2021-05-11 06:31:17', 500, 30.7046486, 76.7178726, '1234567', 10, 20, 10, 1, '', 160, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0),
(31, 'amit', 'kangra', '+640000000000', 'amit@gmail.com', 0, '2021-05-13 07:25:19', '2021-07-21 11:55:11', 500, 32.0668524, 76.3637285, '1234567', 5, 10, 5, 0, '', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0, 0.00, 10.00, 0.00, 0.00, 0.00, 0.00, 0.00, 10),
(32, 'Bhavik', 'Meshinkhub Mohali', '+64855689413689', 'abc@def.com', 0, '2021-05-13 09:58:00', '2021-07-22 22:48:54', 0, 30.6639722, 76.8154888, '123456', 30, 0, 10, 1, '', 1500, 100.00, 200.00, 300.00, 400.00, 100.00, 200.00, 0, 0.00, 100.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0),
(33, 'ankita', 'mohali', '+640987456123', 'admin99@gmail.com', 0, '2021-05-13 10:59:04', '2021-07-05 10:50:05', 0, 30.7046486, 76.7178726, '1234567', 10, 5, 10, 1, '', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 1.46, 0, 0.00, 0.00, 0.00, 0.00, 14.57, 0.00, 0.00, 0),
(50, 'Bhavikgujral', 'MESHINKHUB ZIRAKPUR', '+648556894136', 'demo@vendor.com', 0, '2021-06-29 09:28:35', '2021-07-06 07:49:27', 0, 30.6639722, 76.8154888, '123456', 20, 30, 40, 0, '', 0, 100.00, 200.00, 300.00, 400.00, 100.00, 50.00, 0, 0.00, 100.00, 200.00, 0.00, 20.00, 30.00, 40.00, 0),
(64, 'Bhaviknewvendor', 'Meshinkhub Mohali', '+648146981188', 'abc@demo.com', 0, '2021-07-15 04:46:39', '2021-07-15 04:46:39', 0, 30.6639722, 76.8154888, '123456', 0, 0, 0, 1, '', 1600.5, 100.00, 200.00, 300.00, 400.00, 100.00, 100.00, 1, 100.00, 100.00, 0.00, 0.00, 100.00, 200.00, 300.00, 100),
(70, 'anjana', 'mohali', '+641122336655', 'anjana1@gmail.com', 0, '2021-07-22 10:08:43', '2021-07-22 10:08:43', 0, 30.7046486, 76.7178726, '1234567', 0, 0, 0, 1, '', 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 1, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0),
(72, 'BHAVIKTEST', 'MESHINKHUB MOHALI', '+648556894133', 'ab@demo.com', 0, '2021-08-10 11:39:20', '2021-08-10 11:39:20', 0, 30.6639722, 76.8154888, '123456', 0, 0, 0, 1, '', 0, 100.00, 200.00, 300.00, 400.00, 100.00, 100.00, 1, 100.00, 100.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0),
(73, 'suraj', 'MESHINKHUB MOHALI', '+648558888888', 'suraj@testing.com', 0, '2021-08-11 06:49:34', '2021-08-11 08:10:29', 0, 30.6639722, 76.8154888, '123456', 0, 0, 0, 1, '', 0, 100.00, 200.00, 300.00, 400.00, 100.00, 100.00, 0, 0.00, 100.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_floor_rate`
--

CREATE TABLE `vendor_floor_rate` (
  `id` int(11) NOT NULL,
  `floor` int(11) NOT NULL DEFAULT 1,
  `floor_rate` double NOT NULL DEFAULT 0,
  `vendor_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendor_floor_rate`
--

INSERT INTO `vendor_floor_rate` (`id`, `floor`, `floor_rate`, `vendor_id`) VALUES
(40, 1, 100, 32),
(41, 1, 100, 72),
(42, 2, 200, 72),
(43, 1, 100, 73),
(44, 2, 200, 73);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_registration_request`
--

CREATE TABLE `vendor_registration_request` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `business_name` varchar(255) NOT NULL DEFAULT '',
  `mobile` varchar(20) NOT NULL DEFAULT '',
  `business_category` varchar(255) NOT NULL DEFAULT '',
  `remark` varchar(600) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT 0,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_datetime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_registration_request`
--

INSERT INTO `vendor_registration_request` (`id`, `name`, `business_name`, `mobile`, `business_category`, `remark`, `status`, `datetime`, `update_datetime`) VALUES
(17, 'anjana', 'clothes', '1234567890', 'Food Outlet', 'good', 0, '2021-07-02 10:39:37', '2021-07-02 10:39:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boy_account_request`
--
ALTER TABLE `boy_account_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boy_signin_otp`
--
ALTER TABLE `boy_signin_otp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_max_limit`
--
ALTER TABLE `cash_max_limit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consignment_items`
--
ALTER TABLE `consignment_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_boys`
--
ALTER TABLE `delivery_boys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_boy_cash_deposit`
--
ALTER TABLE `delivery_boy_cash_deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_boy_payout`
--
ALTER TABLE `delivery_boy_payout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_boy_signup_requests`
--
ALTER TABLE `delivery_boy_signup_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `online_orders_assign`
--
ALTER TABLE `online_orders_assign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp_mobile`
--
ALTER TABLE `otp_mobile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_logs`
--
ALTER TABLE `payment_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `push_messages_history`
--
ALTER TABLE `push_messages_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_access`
--
ALTER TABLE `roles_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_login`
--
ALTER TABLE `seller_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `switch_delivery_service_request`
--
ALTER TABLE `switch_delivery_service_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_seller_service_bit`
--
ALTER TABLE `tbl_seller_service_bit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `vendor_floor_rate`
--
ALTER TABLE `vendor_floor_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_registration_request`
--
ALTER TABLE `vendor_registration_request`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boy_account_request`
--
ALTER TABLE `boy_account_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `boy_signin_otp`
--
ALTER TABLE `boy_signin_otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cash_max_limit`
--
ALTER TABLE `cash_max_limit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `consignment_items`
--
ALTER TABLE `consignment_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `delivery_boys`
--
ALTER TABLE `delivery_boys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `delivery_boy_cash_deposit`
--
ALTER TABLE `delivery_boy_cash_deposit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `delivery_boy_payout`
--
ALTER TABLE `delivery_boy_payout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `delivery_boy_signup_requests`
--
ALTER TABLE `delivery_boy_signup_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `online_orders_assign`
--
ALTER TABLE `online_orders_assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=353;

--
-- AUTO_INCREMENT for table `otp_mobile`
--
ALTER TABLE `otp_mobile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=157;

--
-- AUTO_INCREMENT for table `payment_logs`
--
ALTER TABLE `payment_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `push_messages_history`
--
ALTER TABLE `push_messages_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles_access`
--
ALTER TABLE `roles_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `rules`
--
ALTER TABLE `rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `seller_login`
--
ALTER TABLE `seller_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `switch_delivery_service_request`
--
ALTER TABLE `switch_delivery_service_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_seller_service_bit`
--
ALTER TABLE `tbl_seller_service_bit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `vendor_floor_rate`
--
ALTER TABLE `vendor_floor_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `vendor_registration_request`
--
ALTER TABLE `vendor_registration_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
